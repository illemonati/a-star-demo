export enum SpecialState {
    Normal,
    Start,
    End,
    FinalPath,
    Obstacle,
}

export class AStarNode {
    gVal: number;
    hVal: number;
    visited = false;
    inOpenList = false;
    x: number;
    y: number;
    specialState: SpecialState = SpecialState.Normal;
    previous: AStarNode | null = null;
    get fVal(): number {
        return this.gVal + this.hVal;
    }
    constructor(x: number, y: number, gVal?: number, hVal?: number) {
        this.x = x;
        this.y = y;
        this.hVal = hVal || Infinity;
        this.gVal = gVal || Infinity;
    }
}

export const sqrt2 = Math.sqrt(2);

export function calcualteManhattenDistance(node1: AStarNode, node2: AStarNode) {
    const dx = Math.abs(node1.x - node2.x);
    const dy = Math.abs(node1.y - node2.y);
    const d = 1;
    const d2 = sqrt2;
    return d * (dx + dy) + (d2 - 2 * d) * Math.min(dx, dy);
}
